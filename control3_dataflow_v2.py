from __future__ import absolute_import

import argparse
import logging
import re

from past.builtins import unicode

import apache_beam as beam
from apache_beam.io import ReadFromText
from apache_beam.io import WriteToText
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions

import pandas as pd


class SetAtypicals(beam.DoFn): 
    def process(self, element, uuid_list):
        if element['uuid'] in uuid_list:
            element['estado']= 'Atípico mensual'
            element['control'] = 'control-3'
        yield element

class ReviewConditions(beam.DoFn):

    def __init__(self):
        import pandas as pd
        import apache_beam as beam
        self.pd = pd
        self.beam = beam
        
    def process(self, element, R, data):
        (group, values) = element
        data_group = group.split('-')

        station = data_group[0]
        type_station = data_group[1]
        month = data_group[2]
        df_values = self.pd.DataFrame(values)
        q1 = df_values.valores.quantile(0.25)
        q3 = df_values.valores.quantile(0.75)
        c1 = q3 + R*(q3-q1)
        c2 = q1 - R*(q3-q1)

        #FILTRANDO DATOS DE BIG QUERY POR CONDICIÓN
        atypical_data = data | 'Filter atypical data' >> self.beam.Filter(lambda data: ((data['valor_procesado']>c1) or (data['valor_procesado']<c2)) and (str(data['cod_estacion'])==station) and (data['tipo_estacion']==type_station)and (data['mes']==month))
        #atypical_data |  "Print atypicals" >> beam.io.WriteToText('gs://dataflow_controles/outputData/atypical_data_{}.txt'.format(group))

        return atypical_data | 'Return uuid list of atypical data' >> beam.Map(lambda row:row['uuid']) #| beam.combiners.ToList()

def run(argv=None):
    """Main entry point; defines and runs the wordcount pipeline."""
    table = "precipitacion_ID3026"
    #table = 'Temperatura_Maxima_initial'
    #min_date = '1992-03-14 00:00:00.000'
    #max_date = '1997-04-17 23:59:59.999'
    #table = 'Temperatura_Maxima'
    min_date = '1990-01-01 00:00:00.000'
    max_date = '1990-12-31 23:59:59.999'

    with beam.Pipeline(options=PipelineOptions.from_dictionary(argv)) as p:

        #query_filter = """
        #        SELECT variable_clima, tipo_estacion, opcional, cod_estacion, fecha, valor, estado, valor_procesado, uuid, control FROM `servinf-sigra-caf-dev.lake.{0}`\
        #        WHERE cod_estacion in (35015030,32075080)  and EXTRACT(MONTH FROM fecha) IN (3,4) and valor_procesado != 99999 and fecha between '{1}' and '{2}' ORDER BY cod_estacion, tipo_estacion, fecha
        #        """.format(table, min_date, max_date)
        #query_filter = """
        #        SELECT variable_clima, tipo_estacion, opcional, cod_estacion, fecha, valor, estado, valor_procesado, uuid, control FROM `servinf-sigra-caf-dev.lake.{0}`\
        #        WHERE EXTRACT(MONTH FROM fecha) IN (3,4,5,6,7) and valor_procesado != 99999 ORDER BY cod_estacion, tipo_estacion, fecha
        #        """.format(table)
        query_filter = """
                SELECT variable_clima, tipo_estacion, opcional, cod_estacion, fecha, valor, estado, valor_procesado, uuid, control FROM `servinf-sigra-caf-dev.SIGRA.{0}`\
                WHERE fecha IS NOT NULL ORDER BY cod_estacion, tipo_estacion, fecha
                """.format(table)
                
        print('QUERY: ', query_filter)

        #LECTURA DATOS DE BIG QUERY
        BQ_DATA_I = p | 'Read table from BigQuery ' >> beam.io.Read(
                beam.io.BigQuerySource(
                    query=query_filter,
                    use_standard_sql=True))
        #BQ_DATA_I |  "Print bq data" >> beam.io.WriteToText('gs://dataflow_controles/outputData/bq_data.txt')
        
        #FILTRANDO DATOS DE BIG QUERY POR CONDICIÓN
        BQ_DATA = BQ_DATA_I | 'Filter 99999 data' >> beam.Filter(lambda data: data['valor_procesado'] != 99999)
        

        #ACOMODANDO LOS DATOS PARA AGRUPAR
        BQ_DATA_CONDITION = BQ_DATA | 'Map Account to Order Details' >> beam.Map(lambda row: (
                str(row['cod_estacion'])+'-'+ row['tipo_estacion']+'-'+ str(row['fecha'].split('-')[1]), row['valor_procesado']
            ))
        #BQ_DATA_CONDITION |  "Print accommodated data" >> beam.io.WriteToText('gs://dataflow_controles/outputData/accommodated_data.txt')

        #AGRUPANDO LOS DATOS POR ESTACION Y MES
        BQ_DATA_CONDITION2 = ({'valores': BQ_DATA_CONDITION} | 'Group counts per produce' >> beam.CoGroupByKey())
        #BQ_DATA_CONDITION2 |  "Print groupby data" >> beam.io.WriteToText('gs://dataflow_controles/outputData/groupby_data.txt')

        #DATOS DE BQ CON EL MES
        BQ_DATA_CONDITION3 = BQ_DATA | 'add month' >> beam.Map(lambda x: {'tipo_estacion': x['tipo_estacion'], 'cod_estacion': x['cod_estacion'], 'valor_procesado': x['valor_procesado'], 'mes': x['fecha'].split('-')[1], 'uuid': x['uuid'] })
        #BQ_DATA_CONDITION3 |  "Print data with month" >> beam.io.WriteToText('gs://dataflow_controles/outputData/data_month.txt')

        #APLICANDO FUNCION CON LAS CONDICIONES DEL CONTROL 3
        R=5
        BQ_DATA_FINAL = BQ_DATA_CONDITION2 | beam.ParDo(ReviewConditions(), R, beam.pvalue.AsIter(BQ_DATA_CONDITION3))
        BQ_DATA_FINAL |  "Print uuid atypical data after condition" >> beam.io.WriteToText('gs://dataflow_controles/outputData/uuid_atypical.txt')
        
        #UNIENDO LAS LISTAS
        #BQ_DATA_FINAL1 = BQ_DATA_FINAL | beam.ParDo(AppendList())
        #BQ_DATA_FINAL1 |  "Print uuid list" >> beam.io.WriteToText('gs://dataflow_controles/outputData/uuid_list.txt')
        
        #MODIFICANDO COLUMNA ESTADO Y CONTROL DE LOS ATÍPICOS ENCONTRADOS
        FINAL = BQ_DATA_I | beam.ParDo(SetAtypicals(), beam.pvalue.AsList(BQ_DATA_FINAL))
        #FINAL |  "Datos finales con atipicos marcados" >> beam.io.WriteToText('gs://dataflow_controles/outputData/final_data.txt')

        #GUARDANDO DATOS EN TABLA DE BIGQUERY
        FINAL | 'Write Data to BigQuery' >> beam.io.Write(
            beam.io.BigQuerySink(
                'servinf-sigra-caf-dev:lake.precipitacion_final',
                schema='variable_clima:STRING, tipo_estacion:STRING, opcional:STRING, cod_estacion:INTEGER, fecha:TIMESTAMP, valor:FLOAT, estado:STRING, valor_procesado:FLOAT, uuid:STRING, control:STRING',
                create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    
    variable = 'precipitacion'
    PROJECT = 'servinf-sigra-caf-dev'
    BUCKET = 'dataflow_controles'

    pipeline_options = {
        'project': PROJECT,
        'job_name': 'control3-dataflow-workers-pre',
        'staging_location': 'gs://' + BUCKET + '/staging',
        'runner': 'DataflowRunner',
        #'setup_file': './setup.py',
        'disk_size_gb': 150,
        'num_workers':1000,
        #'max_num_workers': 1000,
        'worker_machine_type': 'n1-highmem-96', #'n1-highcpu-96', #
        'job-name': variable,
        'temp_location': 'gs://' + BUCKET + '/temp',
        'region': 'us-central1',
        'experiments': ['shuffle_mode=service']
        #'template_location': 'gs://' + settings.BUCKET + 'dataflow/clima/'+variable+'/templates/'+variable
    }
    run(pipeline_options)